# pointcloud_integrator_

This package integrates input point clouds based on thier relationship described as a graph.
The entire point clouds are also rendered using OpenGL.

This package is originally from lsd_slam_viewer.
Please visit the following URL for more information.
[lsd_slam on GitHub_](https://github.com/tum-vision/lsd_slam)

