#include <list>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
using namespace std;
#include <fstream>

#include <chrono>
#include <unistd.h>


#include <ros/ros.h>
#include <ros/package.h>
#include <ros/time.h>

#include "pointcloud_integrator/scanMsg.h"
#include "pointcloud_integrator/keyScanMsg.h"

//#include <pcl/point_cloud.h>


#include "sophus/sim3.hpp"
#include "sophus/se3.hpp"

#include "pointcloud_integrator/srvLocalization.h"

#include <cstdlib>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/core/core.hpp>

//void location_get(pointcloud_integrator::keyScanMsg& ptsmsg)
void location_get(float *camLocation)
{

  //Eigen::Matrix4f _poseMat;

  
      cout << "Input cam viewpoint >> ";
      cin >> camLocation[0] >> camLocation[1] >> camLocation[2] >>
             camLocation[3] >> camLocation[4] >> camLocation[5] >>
             camLocation[6] >> camLocation[7] >> camLocation[8];

  
  //Sophus::Sim3f _poseSim3(_poseMat);
  //memcpy(ptsmsg.camToWorld.data(), _poseSim3.data(), 7*sizeof(float));
}


int main(int argc, char **argv)
{
 
  ros::init(argc, argv, "location_published_node");
  ros::NodeHandle nh;
  ros::ServiceClient location_client = nh.serviceClient<pointcloud_integrator::srvLocalization>("pointcloud_integrator/location_msg");
  
  //pointcloud_integrator::keyScanMsg _elScanMsg;
  float camLocation[9];
  pointcloud_integrator::srvLocalization srv;
  cv::Mat bmp(cv::Size(600, 400), CV_8UC4);
  
  location_get(camLocation);
  for(int i=0;i<9;i++)
  {
   srv.request.camLocation[i] = camLocation[i];  
  }

  if(location_client.call(srv)){
    memcpy(bmp.data, srv.response.saveImage.data(), srv.response.saveImage.size());
    cv::flip(bmp, bmp, 0);
    if(!cv::imwrite("/home/aisl/Pictures/outputImage2.png", bmp)) printf("capture error! \n");
    //cv::imshow( "window capture", bmp);
    cv::imshow("capture",bmp);
  }

  return 0;
}